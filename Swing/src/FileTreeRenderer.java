import java.awt.Component;
import java.io.File;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class FileTreeRenderer extends DefaultTreeCellRenderer {
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);
		FileWrap fv = (FileWrap) value;
		if (fv.isFile()) {
			setToolTipText(fv.file.length() + " bytes");
		} else {
			int sumFileSize = 0;
			File[] fa = fv.file.listFiles();
			
			if (fa != null) {
				for (int i = 0; i < fa.length; i++) {
					sumFileSize += fa[i].length();
				}
				setToolTipText(sumFileSize + " bytes");
			}
		}
		return this;
	}
}