import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

public class TreeTest implements ActionListener {

	FileTreeModel ftm;
	JComboBox combo;

	static public void main(String args[]) {
		(new TreeTest()).run();
	}
	
	public void run() {
		JFrame f = new JFrame("Pulsar JTree");

		ArrayList<FileWrap> drives = FileWrap.getAllDrives();
		
		String systemDrive = System.getenv("SystemDrive");
		
		ftm = new FileTreeModel();
		
		JTree tree = new JTree(ftm);
		tree.setCellRenderer(new FileTreeRenderer());
		JScrollPane scrollPane = new JScrollPane(tree);
		
		combo = new JComboBox(drives.toArray());
		combo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ftm.setRoot(FileWrap.getAllDrives().get(combo.getSelectedIndex()));
				ftm.refreshTree();			
			}
		});
		
		
		//select system drive as default
		for(int i=0;i<drives.size();i++){
			if (drives.get(i).file.getPath().equals(systemDrive+"\\"))
			{
					ftm.setRoot(drives.get(i));
					combo.setSelectedIndex(i);
					ftm.refreshTree();
			}
		}
		
		//fallback
		if (ftm.getRoot() == null){
			ftm.setRoot(drives.get(0));
			combo.setSelectedIndex(0);
		}
		
		ToolTipManager.sharedInstance().registerComponent(tree);

		f.add(combo, BorderLayout.NORTH);
		f.add(scrollPane, BorderLayout.CENTER);
		
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
