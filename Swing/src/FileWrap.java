import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.filechooser.FileSystemView;

public class FileWrap {

	public File file;

	public FileWrap(File f) {
		this.file = f;
	}
	
	public static ArrayList<FileWrap> getAllDrives() {
        ArrayList<FileWrap> drives = new ArrayList<>();
        
        for(File drive : File.listRoots()) {
        	drives.add(new FileWrap(drive));
        }
        
        Collections.sort(drives, new Comparator(){

			@Override
			public int compare(Object arg0, Object arg1) {
				return ((FileWrap)arg0).toString().compareTo(((FileWrap)arg1).toString());
			}
        	
        });
        
        return drives;
    }

	public boolean isFile() {
		return file.isFile();
	}

	@Override
    public String toString() {
		//getName nem működik root folderen
        FileSystemView v = FileSystemView.getFileSystemView();
        
        if(v.isFileSystemRoot(file)) {
            return file.toString();
        }
        
        return file.getName();
    }

}
