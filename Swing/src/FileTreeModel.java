import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

public class FileTreeModel implements TreeModel {

	ArrayList<TreeModelListener> listeners;
	FileWrap root;

	public FileTreeModel() {
		this.listeners = new ArrayList<TreeModelListener>();
		this.root = null;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners.add(l);
	}

	@Override
	public Object getChild(Object parent, int index) {
		FileWrap file = (FileWrap) parent;
		if (file == null)
			return null;
		else {
			File[] list = file.file.listFiles();
			Arrays.sort(list);
			ArrayList<FileWrap> fw = new ArrayList<FileWrap>();
			for (File f : list) {
				fw.add(new FileWrap(f));
			}
			return fw.get(index);
		}
	}

	@Override
	public int getChildCount(Object parent) {	
		return ((FileWrap)parent).file.listFiles() == null ? 
				0 : ((FileWrap)parent).file.listFiles().length;
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		FileWrap f = (FileWrap) parent;
		FileWrap childfile = (FileWrap) child;
		int n = -1;
		if (f == null || child == null)
			return n;
		else {
			File[] list = f.file.listFiles();
			for (int i = 0; i < list.length; i++) {
				if (list[i].equals(childfile.file))
					return i;
			}
			return n;
		}
	}
	
	public void refreshTree(){
		TreeModelEvent e = new TreeModelEvent(root, new Object[] { root });
        for(TreeModelListener listener : listeners) {
            listener.treeStructureChanged(e);
        }
	}

	public void setRoot(FileWrap root) {
		this.root = root;
	}

	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public boolean isLeaf(Object node) {
		FileWrap f = (FileWrap) node;

		return f.file.isFile();
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners.remove(l);

	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		// TODO Auto-generated method stub		
	}
}
